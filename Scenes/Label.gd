extends Label


# Declare score variables
var Higscore = 0
var Score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	text = "Angle display"


# Called every frame. 'delta' is the elapsed time since the previous frame.
# Tracks and displays the current score
func _process(delta):
	Score = Score + delta
	text = str("Score: ", stepify (Score, 0.1))

# Sets a new high score if old one is worse than current score. Also resets score
func fail():
	if Higscore < Score:
		Higscore = Score
		$HSboard.text = str("Highscore: ", stepify (Higscore, 0.1))
	Score = 0
	
