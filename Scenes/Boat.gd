extends KinematicBody2D

# For saving reference to the scoreboard
var Scoreboard

# Called when the object is created
func _ready():
	Scoreboard = get_node('../Scorenode/Scoreboard') # Saves a reference to the scoreboard node

# Called every physics frame. turns the boat accordign to device gravity sensor
func _physics_process(delta):
	if abs(Input.get_gravity().y) < 3:
		rotation = -0.1 - (Input.get_gravity().y / 4.3)

# Dlivers info of failure to scoreboard
func collision():
	Scoreboard.fail()
	
