extends Node2D
export (PackedScene) var timer

# Saving wave-scene for later use
var Wave = preload("res://Scenes/Wave.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	# randomizes the random_range seed
	randomize()
	#Create and start a timer to call _on_timer_timeout every 2 seconds
	timer = Timer.new()
	timer.connect("timeout",self,"_on_timer_timeout") 
	add_child(timer)
	timer.set_wait_time( 2 )
	timer.start()

# Spawns a wave and randomizes the wave spawn intervall
func _on_timer_timeout():
	add_child(Wave.instance())
	timer.set_wait_time( rand_range(0.3, 2) )
