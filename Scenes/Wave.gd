extends KinematicBody2D


# Setting wave speed and velocity as variables
var Velocity = Vector2.ZERO
var Speed = 100

# Called when the node enters the scene tree for the first time.
# Sets the direction and size for the wave randomly
func _ready():
	scale.y = rand_range(0.1, 0.25)
	position.x = position.x + (0.2 - scale.y) * 100
	Velocity.y = [-1,1][randi() % 2]
	# Checks which way the wave is travelling and sets the position and
	# alignment accordingly
	if Velocity.y == 1:
		position.y = -400
	else:
		position.y = 400
		scale.x = -scale.x

# Called every frame
func _physics_process(delta):
	# Moves the wave and saves the colliosion to a variable if hits something
	var collision_object = move_and_collide(Velocity * Speed * delta)
	if collision_object: # Called if hit is detected
		# Checks if collider has collision method (aka. is the boat)
		if collision_object.collider.has_method("collision"):
			collision_object.collider.collision()
		queue_free() # Destroys the wave after colliding
